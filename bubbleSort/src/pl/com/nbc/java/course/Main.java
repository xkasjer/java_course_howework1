package pl.com.nbc.java.course;

import java.util.Arrays;

public class Main {
    public static void main(String... args){
        int[] table = {1,4,2,55,10,7};
        System.out.println(Arrays.toString(table));
        BubbleSort sorter = new BubbleSort();
        System.out.println("After sort:");
        System.out.println(Arrays.toString(sorter.sort(table)));

    }
}
