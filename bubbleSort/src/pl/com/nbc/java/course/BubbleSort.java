package pl.com.nbc.java.course;

public class BubbleSort {

    public int[] sort(int[] table) {
        int tmp;
        for (int a = 0; a < table.length; a++) {
            for (int b = 0; b < table.length; b++) {
                if (table[a] < table[b]) {
                    tmp = table[a];
                    table[a] = table[b];
                    table[b] = tmp;
                }
            }
        }
        return table;
    }
}
