package pl.com.nbc.course.java;

import java.util.Arrays;

public class Main {


    public static void main(String... args) {

        int array[] = {12, 11, 13, 5, 6, 7};

        System.out.println(Arrays.toString(array));

        MergeSort mergeSort = new MergeSort();
        mergeSort.sort(array);

        System.out.println(Arrays.toString(array));

    }
}
