package pl.com.nbc.course.java;

class MergeSort {

    void sort(int array[]) {
        cut(array, 0, array.length - 1);
    }

    private void merge(int array[], int l, int c, int r) {

        int i = 0, j = 0;
        int leftLength = c - l + 1;
        int rightLength = r - c;
        int lArray[] = new int[leftLength];
        int rArray[] = new int[rightLength];

        System.arraycopy(array, l, lArray, 0, leftLength);
        System.arraycopy(array, c + 1, rArray, 0, rightLength);

        int k = l;
        while (i < leftLength && j < rightLength) {
            if (lArray[i] <= rArray[j]) {
                array[k] = lArray[i];
                i++;
            } else {
                array[k] = rArray[j];
                j++;
            }
            k++;
        }

        while (i < leftLength) {
            array[k] = lArray[i];
            i++;
            k++;
        }

        while (j < rightLength) {
            array[k] = rArray[j];
            j++;
            k++;
        }
    }

    private void cut(int array[], int left, int right) {
        if (left < right) {
            int center = (left + right) / 2;

            cut(array, left, center);
            cut(array, center + 1, right);
            merge(array, left, center, right);
        }
    }
}