package pl.com.nbc.java.course;

import pl.com.nbc.java.course.car.type.CarTypeInterface;
import pl.com.nbc.java.course.car.type.TruckType;

public class Truck extends Vehicle {

    private int seats;
    private int doors;

    public Truck(String name, int weight, int seats, int doors, CarTypeInterface type) {
        super(name, weight, type);
        this.doors = doors;
        this.seats = seats;
    }

    public int getSeats() {
        return seats;
    }

    public Truck setSeats(int seats) {
        this.seats = seats;
        return this;
    }

    public int getDoors() {
        return doors;
    }

    public Truck setDoors(int doors) {
        this.doors = doors;
        return this;
    }

    @Override
    public String toString() {
        return "Car{" +
                "seats=" + seats +
                ", doors=" + doors +
                ", weight=" + getWeight() +
                ", name=" + getName() +
                ", type=" + getType() +
                '}';
    }

    public static class Builder {
        private String name;
        private int seats;
        private int doors;
        private int weight;
        private int load;

        public static Builder builder() {
            return new Builder();
        }

        CarTypeInterface type = new TruckType(load);

        public Truck build() {
            return new Truck(name, weight, seats, doors, type);
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setSeats(int seats) {
            this.seats = seats;
            return this;
        }

        public Builder setDoors(int doors) {
            this.doors = doors;
            return this;
        }

        public Builder setWeight(int weight) {
            this.weight = weight;
            return this;
        }

        public Builder setLoad(int load) {
            this.load = load;
            return this;
        }
    }
}
