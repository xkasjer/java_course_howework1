package pl.com.nbc.java.course.car.type;

abstract class Type implements CarTypeInterface {
    private String name;

    Type(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Type setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return "Type{" +
                "name='" + name + '\'' +
                '}';
    }
}
