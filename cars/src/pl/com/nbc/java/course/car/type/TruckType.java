package pl.com.nbc.java.course.car.type;

public class TruckType extends Type {

    private int load;

    public TruckType(int load) {
        super("TruckType");
        this.load = load;
    }
    public TruckType() {
        super("TruckType");
        this.load = 0;
    }

    @Override
    public String toString() {
        return "TruckType{" +
                "load=" + load +
                ", name=" + getName()+
                '}';
    }
}
