package pl.com.nbc.java.course.car.type;

public interface CarTypeInterface {
    public String getName();
}
