package pl.com.nbc.java.course;

import pl.com.nbc.java.course.car.type.CarTypeInterface;

public interface VehicleInterface {
    String getName();
    Vehicle setName(String name);
    int getWeight();
    Vehicle setWeight(int weight);
    CarTypeInterface getType();
    Vehicle setType(CarTypeInterface type);
}
