package pl.com.nbc.java.course;

import pl.com.nbc.java.course.car.type.CarTypeInterface;

public class Car extends Vehicle {

    private int seats;
    private int doors;

    public Car(String name, int weight, int seats, int doors, CarTypeInterface type) {
        super(name, weight, type);
        this.doors = doors;
        this.seats = seats;
    }

    public int getSeats() {
        return seats;
    }

    public Car setSeats(int seats) {
        this.seats = seats;
        return this;
    }

    public int getDoors() {
        return doors;
    }

    public Car setDoors(int doors) {
        this.doors = doors;
        return this;
    }

    @Override
    public String toString() {
        return "Car{" +
                "seats=" + seats +
                ", doors=" + doors +
                ", weight=" + getWeight() +
                ", name=" + getName() +
                ", type=" + getType()+
                '}';
    }

    public static class Builder {
        private String name;
        private int seats;
        private int doors;
        private int weight;
        private CarTypeInterface type;

        public static Builder builder() {
            return new Builder();
        }

        public Car build() {
            return new Car(name, weight, seats, doors,type);
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setSeats(int seats) {
            this.seats = seats;
            return this;
        }

        public Builder setDoors(int doors) {
            this.doors = doors;
            return this;
        }

        public Builder setWeight(int weight) {
            this.weight = weight;
            return this;
        }

        public Builder setType(CarTypeInterface type) {
            this.type = type;
            return this;
        }
    }
}
