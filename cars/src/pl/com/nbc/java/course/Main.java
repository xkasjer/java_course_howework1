package pl.com.nbc.java.course;

import pl.com.nbc.java.course.car.type.PersonalCar;

import java.util.Arrays;

public class Main {

    public static void main(String... args) {
        VehicleInterface[] vehicles = new VehicleInterface[2];

        Car.Builder carBuilder = Car.Builder.builder();

        Vehicle car = carBuilder
                .setDoors(5)
                .setName("Fiat")
                .setSeats(5)
                .setWeight(1200)
                .setType(new PersonalCar())
                .build();
        vehicles[0] = car;
        System.out.println(car);

        Truck.Builder truckBuilder = Truck.Builder.builder();
        Vehicle truck = truckBuilder
                .setDoors(2)
                .setName("Man")
                .setSeats(3)
                .setWeight(10000)
                .setLoad(1500)
                .build();
        System.out.println(truck);
        vehicles[1] = truck;

        System.out.println(Arrays.toString(vehicles));
    }
}