package pl.com.nbc.java.course;


import pl.com.nbc.java.course.car.type.CarTypeInterface;

abstract class Vehicle implements VehicleInterface {
    private String name;
    private int weight;
    private CarTypeInterface type;

    Vehicle(String name, int weight, CarTypeInterface type) {
        this.name = name;
        this.weight = weight;
        this.type = type;

    }

    public String getName() {
        return name;
    }

    public Vehicle setName(String name) {
        this.name = name;
        return this;
    }

    public int getWeight() {
        return weight;
    }

    public Vehicle setWeight(int weight) {
        this.weight = weight;
        return this;
    }

    public CarTypeInterface getType() {
        return type;
    }

    public Vehicle setType(CarTypeInterface type) {
        this.type = type;
        return this;
    }
}
