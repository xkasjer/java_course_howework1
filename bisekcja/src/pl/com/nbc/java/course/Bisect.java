package pl.com.nbc.java.course;

public class Bisect {

    private FunctionInterface function;

    public Bisect(FunctionInterface function) {
        this.function = function;
    }

    public double find(double epsilon, double a, double b) throws Exception {
        if(!(function.f(a) * function.f(b) < 0) ){
            throw new Exception("Function has not zero place");
        }
        double x1;
        while (Math.abs(a - b) > epsilon) {
            x1 = (a + b) / 2;

            if (function.f(x1) * function.f(a) < 0) {
                b = x1;
            } else if (function.f(x1) * function.f(b) < 0) {
                a = x1;
            } else {
                break;
            }

        }
        return (a + b) / 2;
    }


}
