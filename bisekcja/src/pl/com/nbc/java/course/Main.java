package pl.com.nbc.java.course;

public class Main {

    public static void main(String... args) {
        double a = -2L;
        double b = 10L;
        int precision = 3;
        double epsilon = 1;
        for (int i=1;i<=precision;i++){
            epsilon/=10;
        }

        FunctionInterface function = new LineFunction();

        Bisect bisect = new Bisect(function);

        try {
            System.out.printf("%."+String.valueOf(precision)+"f",bisect.find(epsilon,a,b));
        } catch (Exception e) {
            System.out.println("Funkcja nie ma miejsca zerowego w tym przedziale");
        }

    }
}
