package pl.com.nbc.java.course;

public class LineFunction implements FunctionInterface {
    @Override
    public double f(double x) {
        return x*x*x - x +1;
    }
}
