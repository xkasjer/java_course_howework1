package pl.com.nbc.java.course;

public class Fibonacci {

    public int[] generate(int count) {
        int[] tab = new int[count + 1];
        for (int i = 0; i <= count; i++) {
            if (i < 2) {
                tab[i] = i;
            } else {
                tab[i] = tab[i - 1] + tab[i - 2];
            }

        }
        return tab;
    }
}
