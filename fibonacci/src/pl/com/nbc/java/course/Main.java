package pl.com.nbc.java.course;

import java.util.Arrays;

public class Main {

    public static void main(String... args) {

        try {
            int count = Integer.parseInt(args[0]);
            Fibonacci fibonacci = new Fibonacci();
            System.out.println(Arrays.toString(fibonacci.generate(count)));
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Application required 1 parameter");
        } catch (NumberFormatException e) {
            System.out.println("Parameter must be integer");
        }


    }
}
